#!/usr/bin/env bash
echo "***********************************************"
echo "*                                             *"
echo "*                 Install                     *"
echo "*                                             *"
echo "***********************************************"
echo " "

echo "***********************************************"
echo "*                                             *"
echo "*           apt update and upgrade            *"
echo "*                                             *"
echo "***********************************************"
echo " "
apt-get -y update

echo " "
echo "***********************************************"
echo "*                                             *"
echo "*                 Utilities                   *"
echo "*                                             *"
echo "***********************************************"
echo " "

apt-get -y install python3-pip  python3-dev python3-setuptools libpq-dev git nodejs-legacy npm
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash

rbenv install 2.3.1
rbenv global 2.3.1
gem install sass
npm -g install gulp


echo " "
echo "***********************************************"
echo "*             List dependencies               *"
echo "***********************************************"
echo " "
echo " "
echo "***********************************************"
echo "*                                             *"
echo "*  install dependencies (including django)    *"
echo "*                                             *"
echo "***********************************************"
echo " "
pip3 install --upgrade pip
pip3 install -r requirements/production.txt

echo " "
echo "***********************************************"
echo "*             List dependencies               *"
echo "***********************************************"
echo " "
pip3 freeze

echo " "
echo "***********************************************"
echo "*          Install npm dependencies           *"
echo "***********************************************"
echo " "
npm install
gulp build
